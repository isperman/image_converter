from PIL import Image
import os

source_path = "./"
output_dir = "./output/"
source_extension = '.tif'
output_extension = '.jpg'
output_quality = 95

resize_factor = 1

if not os.path.isdir(output_dir):
    os.mkdir(output_dir)

for fl in os.listdir(source_path):
    if fl.endswith(source_extension):
        print("type: {}, file: {}".format(type(fl), fl))
        complete_path = os.path.join(source_path, fl)
        fl_output = fl.replace(source_extension, output_extension)
        output_path = os.path.join(output_dir, fl_output)

        print(output_path)

        img = Image.open(complete_path)
        (width, height) = img.size
        new_size = (int(width*resize_factor), int(height*resize_factor))
        img_resized = img.resize(new_size)
        img_resized.save(output_path, quality=output_quality)
